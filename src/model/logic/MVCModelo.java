package model.logic;
import java.io.FileReader;
import java.util.ArrayList;

import com.opencsv.CSVReader;
import model.data_structures.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */

	public final static String SHELLSORT = "ShellSort";
	public final static String MERGESORT = "MergeSort";
	public final static String QUICKSORT = "QuickSort";

	private ILinkedList<UBERTrip> linkedList;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */

	public MVCModelo()
	{
		linkedList = new LinkedList<UBERTrip>();
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int size()
	{
		return linkedList.getSize();
	}

	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public UBERTrip getLast()
	{
		return linkedList.getLast().getObject();
	}

	public UBERTrip getFirst () {
		return linkedList.getFirst().getObject();
	}


	public int cargarCSV(String pRutaArchivo)
	{
		int contadorLineas = 0;
		try {
			//Reader que lee el csv linea por linea
			CSVReader reader = new CSVReader(new FileReader(pRutaArchivo));
			//retorna un arreglo de arreglos de strings
			for(String[] linea : reader) 
			{
				if(contadorLineas != 0) //cuando es 0 est� leyendo la primera l�nea.
				{	
					linkedList.append(new Node<UBERTrip>(
							(new UBERTrip(
									Integer.parseInt(linea[0]),
									Integer.parseInt(linea[1]), 
									Integer.parseInt(linea[2]), 
									Double.parseDouble(linea[3]), 
									Double.parseDouble(linea[4]),
									Double.parseDouble(linea[5]),
									Double.parseDouble(linea[6])))));
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return --contadorLineas;
	}

	/*Consultar los viajes de una hora dada.
	 * a. El m�todo debe retornar los viajes resultantes en una representaci�n tipo arreglo de
	 *  objetos
	 *	Comparables.
	 *	b. Los viajes consultados deben mantenerse en la estructura de datos de viajes.
	 *	c. Como informaci�n al usuario debe mostrarse el n�mero de viajes resultantes de la consulta
	 */
	public Comparable<UBERTrip>[] getTripsGivenHour(int pHod)
	{
		ILinkedList<UBERTrip> trips = new LinkedList<UBERTrip>();
		
		Node<UBERTrip> current = null;
		
		current = linkedList.getFirst();
		
		while(current != null)
		{
			UBERTrip uber = null;
			uber = current.getObject();
			if(uber.getHod() == pHod)
			{
				Node<UBERTrip> add = new Node<UBERTrip>(uber);
				trips.append(add);
			}
			current = current.getNext();
		}

		UBERTrip[] array = new UBERTrip[trips.getSize()];
		Node<UBERTrip> add = trips.getFirst();
		int i = 0;
		while(add != null && i < array.length)
		{
			array[i] = add.getObject();
			add = add.getNext();
			i++;
		}

		return array;
	}

	public long sortingBenchmarkOptionA(Comparable[] array, String sort)
	{
		switch(sort)
		{
		case SHELLSORT:
			long shellStart = System.currentTimeMillis();
			ShellSort.sort(array);
			long shellEnd = System.currentTimeMillis();
			return shellEnd - shellStart;
		case QUICKSORT:
			long quickStart = System.currentTimeMillis();
			QuickSort.sort(array);
			long quickEnd = System.currentTimeMillis();
			return quickEnd - quickStart;
		case MERGESORT:
			long mergeStart = System.currentTimeMillis();
			MergeSort.sort(array);
			long mergeEnd = System.currentTimeMillis();
			return mergeEnd - mergeStart;
		}
		return 0;
	}
}
