package model.logic;

public class UBERTrip implements Comparable<UBERTrip> {
	

	private int sourceid;
	private int dstid;
	private int hod;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;

	public UBERTrip(int pSourceid,int pDstid,int pHod,double pMean_travel_time,double pStandard_deviation_travel_time,double pGeometric_mean_travel_time,double pGeometric_standard_deviation_travel_time)
	{
		sourceid = pSourceid;
		dstid = pDstid;
		hod = pHod;
		mean_travel_time = pMean_travel_time;
		standard_deviation_travel_time = pStandard_deviation_travel_time;
		geometric_mean_travel_time = pGeometric_mean_travel_time;
		geometric_standard_deviation_travel_time = pGeometric_standard_deviation_travel_time;
	}

	public int getSourceid() {
		return sourceid;
	}

	public int getDstid() {
		return dstid;
	}

	public int getHod() {
		return hod;
	}

	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	public double getGeometric_mean_travel_time() {
		return geometric_mean_travel_time;
	}

	public double getGeometric_standard_deviation_travel_time() {
		return geometric_standard_deviation_travel_time;
	}

	@Override
	public int compareTo(UBERTrip parametro) {
		/*
		 *  La comparacion natural (metodo compareTo()) de esta clase debe
		 *	hacerse por su mean_travel_time. Si el mean_travel_time de los viajes comparados es igual, la
		 *	comparacion la resuelve su standard_deviation_travel_time.
		 */
		double meanTime = parametro.getMean_travel_time();
		if(mean_travel_time > meanTime) return 1;
		else if(mean_travel_time < meanTime) return -1;
		else if(mean_travel_time == meanTime)
		{
			double desviacion = parametro.getStandard_deviation_travel_time();
			if(standard_deviation_travel_time > desviacion) return 1;
			else if(standard_deviation_travel_time < desviacion) return -1;
		}
		return 0;
	}

	@Override
	public String toString() {
		return "\t Id de origen:"+ sourceid +"\n"+
		"\t Id de destino:"+ dstid +"\n"+
		"\t Hora del d�a:" + hod +"\n"+
		"\t Tiempo promedio de viaje:" + mean_travel_time +"\n"+
		"\t Desviaci�n est�ndar:"+ standard_deviation_travel_time +"\n"+
		"\t Tiempo promedio geom�trico:"+ geometric_mean_travel_time +"\n"+
		"\t Desviaci�n est�ndar geom�trica:"+ geometric_standard_deviation_travel_time +"\n";
		
	}
	
}
