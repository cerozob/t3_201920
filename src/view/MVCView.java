package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar archivo del segundo trimestre de 2018.");
			System.out.println("2. Consultar viajes en una Hora.");
			System.out.println("3. Ordenar consulta ascendentemente con ShellSort.");
			System.out.println("4. Ordenar consulta ascendentemente usando MergeSort.");
			System.out.println("5. Ordenar consulta ascendentemente usando QuickSort.");
			System.out.println("6. Exit.");
			System.out.println("1234567890. realizar punto 8c");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
