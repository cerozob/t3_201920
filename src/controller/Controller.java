package controller;

import java.util.Scanner;

import model.logic.*;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	private Comparable<UBERTrip>[] consulta;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Cargar Archivo del segundo trimestre de 2018");
				modelo = new MVCModelo(); 
				//    "./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"
				int lineas = modelo.cargarCSV("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv");
				System.out.println("Total de datos cargados: "+lineas);

				UBERTrip primerViaje = modelo.getFirst();
				UBERTrip ultimoViaje = modelo.getLast();
				System.out.println("Origen del primer viaje: "+primerViaje.getSourceid());
				System.out.println("Destino del primer viaje: "+primerViaje.getDstid());
				System.out.println("Hora del primer viaje: "+primerViaje.getHod());
				System.out.println("Tiempo promedio del primer Viaje: "+primerViaje.getMean_travel_time());
				System.out.println("----------------------------------------------------------------------------------");
				System.out.println("Origen del ultimo viaje: "+ultimoViaje.getSourceid());
				System.out.println("Destino del ultimo viaje: "+ultimoViaje.getDstid());
				System.out.println("Hora del ultimo viaje: "+ultimoViaje.getHod());
				System.out.println("Tiempo promedio del ultimo Viaje: "+ultimoViaje.getMean_travel_time());
				break;

			case 2:
				System.out.println("Ingrese la hora a consultar: ");
				int hora = lector.nextInt();
				if(hora < 24 && hora >= 0) //La mayor hora es 23
				{
					consulta = modelo.getTripsGivenHour(hora);
					System.out.println("Se encontraron "+consulta.length+" viajes a las "+hora+" horas.");
				}
				else
				{
					System.out.println("Hora inv�lida");
				}				
				break;
				/*
				 * Como informaci�n al usuario debe mostrarse: el tiempo en milisegundos que tom�
				 * el algoritmo realizando el ordenamiento, los 10 primeros viajes y los 10 �ltimos
				 * viajes resultado del ordenamiento.
				 */
			case 3:
				System.out.println("3. Ordenar consulta ascendentemente con ShellSort");
				if(consulta != null)
				{
					long tiempo = modelo.sortingBenchmarkOptionA(consulta,
							model.logic.MVCModelo.SHELLSORT );
					System.out.println("Primeros 10 viajes");
					for(int i = 0; i <= 10 ;i++)
					{
						System.out.println("------------------------------------------------------------------------------------------------------------");
						System.out.println(consulta[i].toString());
						System.out.println("------------------------------------------------------------------------------------------------------------");

					}
					System.out.println("�ltimos 10 viajes");
					for(int i = consulta.length-1; i >= (consulta.length+10) ;i--)
					{
						System.out.println("------------------------------------------------------------------------------------------------------------");
						System.out.println(consulta[i].toString());
						System.out.println("------------------------------------------------------------------------------------------------------------");

					}
					System.out.println("El ordenamiento tom� "+tiempo+" milisegundos");
				}
				else
				{
					System.out.println("No se ha realizado la consulta");
				}
				break;

			case 4:
				System.out.println("4. Ordenar consulta ascendentemente usando MergeSort");
				if(consulta != null)
				{
					long tiempo = modelo.sortingBenchmarkOptionA(consulta,
							model.logic.MVCModelo.MERGESORT );
					System.out.println("Primeros 10 viajes");
					for(int i = 0; i <= 10 ;i++)
					{
						System.out.println("------------------------------------------------------------------------------------------------------------");
						System.out.println(consulta[i].toString());
						System.out.println("------------------------------------------------------------------------------------------------------------");

					}
					System.out.println("�ltimos 10 viajes");
					for(int i = consulta.length-1; i >= (consulta.length+10) ;i--)
					{
						System.out.println("------------------------------------------------------------------------------------------------------------");
						System.out.println(consulta[i].toString());
						System.out.println("------------------------------------------------------------------------------------------------------------");

					}
					System.out.println("El ordenamiento tom� "+tiempo+" milisegundos");
				}
				else
				{
					System.out.println("No se ha realizado la consulta");
				}
				break;

			case 5: 
				System.out.println("5. Ordenar consulta ascendentemente usando QuickSort");
				if(consulta != null)
				{
					long tiempo = modelo.sortingBenchmarkOptionA(consulta,
							model.logic.MVCModelo.QUICKSORT );
					System.out.println("Primeros 10 viajes");
					for(int i = 0; i <= 10 ;i++)
					{
						System.out.println("------------------------------------------------------------------------------------------------------------");
						System.out.println(consulta[i].toString());
						System.out.println("------------------------------------------------------------------------------------------------------------");
					}
					System.out.println("�ltimos 10 viajes");
					for(int i = consulta.length-1; i >= (consulta.length+10) ;i--)
					{
						System.out.println("------------------------------------------------------------------------------------------------------------");
						System.out.println(consulta[i].toString());
						System.out.println("------------------------------------------------------------------------------------------------------------");

					}
					System.out.println("El ordenamiento tom� "+tiempo+" milisegundos");
				}
				else
				{
					System.out.println("No se ha realizado la consulta");
				}
				break;	

			case 6: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	
				
			case 1234567890:
				System.out.println("8C");
				System.out.println("Comparaci�n de tiempos para conjuntos de datos");
				System.out.println("\t Hora \t Nviajes \t Shellsort \t Mergesort \t Quicksort");
				Long timeShell;
				Long timeMerge;
				Long timeQuick;
				for(int i = 0;i < 6 ;i++)
				{
					int horaBuscar = (3*(i+1)+1);
					consulta = modelo.getTripsGivenHour(horaBuscar);
					timeShell = modelo.sortingBenchmarkOptionA(consulta,model.logic.MVCModelo.SHELLSORT );
					consulta = modelo.getTripsGivenHour(horaBuscar);
					timeMerge = modelo.sortingBenchmarkOptionA(consulta,model.logic.MVCModelo.MERGESORT );
					consulta = modelo.getTripsGivenHour(horaBuscar);
					timeQuick = modelo.sortingBenchmarkOptionA(consulta,model.logic.MVCModelo.QUICKSORT );
					System.out.println("\t "+horaBuscar+" \t "+consulta.length+" \t \t "+timeShell+" \t \t "+timeMerge+" \t \t "+timeQuick);
				}			
				
				
				
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
